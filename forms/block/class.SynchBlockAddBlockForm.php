<?php

class SynchBlockAddBlockForm extends SynchForm {

  public function getFormId() {
    return "block_add_block_form";
  }

  public function createInstance() {
    return new SynchBlockAddBlockFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Creating new custom block');
  }

  public function getHelp() {
    return t('Adds a new block');
  }

  public function getHelpExceptions() {
    return array();
  }

}

class SynchBlockAddBlockFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'block') . '/block.admin.inc');
  }

  public function getLocalValues() {
    return parent::getLocalValues() + array('module', 'delta');
  }

  public function preExec() {
    if (!parent::preExec()) return false;
    parent::preExec();

    return true;
  }

  public function preSave($finalFormState) {
    parent::preSave($finalFormState);

    if ($finalFormState['values']['module'] == 'block') {
      //Save custom delta for later use
      $this->info['new_delta'] = (int) $finalFormState['values']['delta'];
    }
    $this->info['new_bid'] = (int) $this->getBid($finalFormState['values']['module'], $finalFormState['values']['delta']);
  }

  /**
   * Returns the block id of the specified block
   * @return int
   */
  private function getBid($module, $delta) {
    $query = db_select('block', 'i');
    $query->addField('i', 'bid');
    $query->condition('module', $module)
      ->condition('delta', $delta);
    $result = $query->execute();
    $record = $result->fetchAssoc();
    if (!$record) {
      throw new Exception('Block does not exist in database: ' . $module . ',' . $delta);
    }
    return $record['bid'];
  }

  public function skip() {
    parent::skip();

    if (isset($this->info['new_delta'])) {
      $this->source->setLocalId('block_custom', $this->info['new_delta'], NULL);
    }

    if (isset($this->info['new_bid'])) {
      $this->source->setLocalId('block', $this->info['new_bid'], NULL);
    }
  }

  public function postExec($formState) {
    parent::postExec($formState);

    //Post process
    if (isset($this->info['new_delta'])) {
      //Translate delta
      $this->source->setLocalId('block_custom', $this->info['new_delta'], $formState['values']['delta']);
    }

    if (isset($this->info['new_bid'])) {
      //Translate bid
      $bid = (int) $this->getBid($formState['values']['module'], $formState['values']['delta']);
      $this->source->setLocalId('block', $this->info['new_bid'], $bid);
    }
  }

  //@todo: localize roles and content types

  public function getAction() {
    return url('admin/structure/block/add');
  }

}