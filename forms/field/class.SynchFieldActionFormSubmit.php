<?php

abstract class SynchFieldActionFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'field_ui') . '/field_ui.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Convert remote id to local id
    $field =& $this->formState['build_info']['args'][0];
    $local_instance_id = $this->source->getLocalId('field_instance', $field['id']);

    //Check existance
    $results = db_select('field_config_instance', 's')
      ->fields('s')
      ->condition('id', $local_instance_id)
      ->condition('deleted', 0)
      ->execute();
    $record = $results->fetchObject();

    if (!$record) {
      //Field instance does not exist! Don't submit form as it would result in an error
      drupal_set_message(t('Form #!id not submitted because field instance does not exist: \'!fiid.\'', array('!id' => $this->formId, '!fiid' => $field['id'])), 'warning');
      return false;
    }

    // Localize the complete field
    $field = field_info_instance($record->entity_type, $record->field_name, $record->bundle);

    return true;
  }

  public function getAction() {
    $action = $this->form['#action'];

    //@see field_ui_menu
    $field = $this->formState['build_info']['args'][0];
    $type_info = entity_get_info($field['entity_type']);
    $bundle_info = $type_info['bundles'][$field['bundle']];

    $path = $bundle_info['admin']['path'];
    $path_parts = explode('/', $path);

    $path_parts[$bundle_info['admin']['bundle argument']] = $field['bundle'];

    $path_parts[] = 'fields';
    $path_parts[] = $field['field_name'];
    $path_parts[] = $this->getActionType();

    return url(implode('/', $path_parts));
  }

  /**
   * Returns the last part of the action path
   * @return string
   */
  public abstract function getActionType();

}