<?php

require_once("class.SynchFieldEditForm.php");

class SynchFieldSettingsForm extends SynchFieldEditForm {

  public function getFormId() {
    return "field_ui_field_settings_form";
  }

  public function createInstance() {
    return new SynchFieldSettingsFormSubmit();
  }

  public function getHelp() {
    return t('Edits the general field settings for all instances');
  }

}

require_once("class.SynchFieldActionFormSubmit.php");
class SynchFieldSettingsFormSubmit extends SynchFieldActionFormSubmit {

  public function getActionType() {
    return "field-settings";
  }

}