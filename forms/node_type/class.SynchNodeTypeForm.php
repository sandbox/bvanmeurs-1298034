<?php

class SynchNodeTypeForm extends SynchForm {

  public function getFormId() {
    return "node_type_form";
  }

  public function createInstance() {
    return new SynchNodeTypeFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    $args = $formState['build_info']['args'];
    if (count($args) == 0) {
      //New node type
      return t('Add new node type');
    } else {
      //Configure node type
      $typeObj = $args[0];
      return t('Configuring node type \'!id\' with name \'!name\'', array('!id' => $typeObj->type, '!name' => $typeObj->name));
    }
  }

  public function getHelp() {
    return t('Adds or edits a node type');
  }

  public function getHelpExceptions() {
    return array(
      t('if the node type does not exist locally, an error is shown'),
      t('if one of the enabled menus does not exist locally, it is ignored a warning is shown'),
      t('if the parent menu does not exist locally, either the default one (if it is possible) or no item is set and a warning is shown'),
    );
  }

}

class SynchNodeTypeFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'node') . '/content_types.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    $args = $this->formState['build_info']['args'];

    if (count($args) == 0) {
      //New node type
      $type = $this->formState['values']['type'];
    } else {
      //Configure node type
      $type = $this->source->getLocalId('node_type', $args[0]->type);

      //Check if block exists
      $types = node_type_get_types();
      if (!array_key_exists($type, $types)) {
        //Node type does not exist! Don't submit form as it would result in an error
        drupal_set_message(t('Can\'t submit form #!id not submitted because node type does not exist: \'!type\'', array('!id' => $this->id, '!type' => $type)), 'warning');
        return false;
      }

      $this->formState['build_info']['args'][0] = $types[$type];
    }

    return true;
  }

  public function skip() {
    parent::skip();

    $args = $this->formState['build_info']['args'];
    if (count($args) == 0) {
      //New node type: set to 'no local equivalent'
      $type = $this->formState['values']['type'];
      $this->source->setLocalId('node_type', $type, NULL);
    }
  }

  public function getAction() {
    if (count($this->formState['build_info']['args']) == 0) {
      return url("admin/structure/types/add");
    } else {
      return url("admin/structure/types/manage/{$this->formState['build_info']['args'][0]->type}");
    }
  }

}