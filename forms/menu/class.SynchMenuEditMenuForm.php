<?php

class SynchMenuEditMenuForm extends SynchForm {

  public function getFormId() {
    return "menu_edit_menu";
  }

  public function createInstance() {
    return new SynchMenuEditMenuFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    if ($formState['build_info']['args'][0] == 'add') {
      return t('Adding a new menu');
    } else {
      return t('Editing menu !title', array('!title' => $formState['build_info']['args'][1]['menu_name']));
    }
  }

  public function getHelp() {
    return t('Edits or adds a menu');
  }

  public function getHelpExceptions() {
    return array(
      t('if the menu doesn\'t exist locally, an error is shown'),
    );
  }

}

class SynchMenuEditMenuFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'menu') . '/menu.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Check and reset build info
    if ($this->formState['build_info']['args'][0] == 'edit') {
      $menuName = $this->source->getLocalId('menu', $this->formState['build_info']['args'][1]['menu_name']);
      $menu = menu_load($menuName);
      if (!$menu) {
        drupal_set_message(t('Form #!id not submitted because menu does not exist: \'!menu_name\'', array('!id' => $this->id, '!menu_name' => $menuName)), 'warning');
        return false;
      }
      $this->formState['build_info']['args'][1] = $menu;
    }
    return true;
  }

  public function skip() {
    parent::skip();

    if ($this->formState['build_info']['args'][0] == 'add') {
      //Menu has no local equivalent
      $this->source->setLocalId('menu', $this->formState['values']['menu_name'], NULL);
    }
  }

  public function getAction() {
    if ($this->formState['build_info']['args'][0] == 'add') {
      return url("admin/structure/menu/add");
    } else {
      return url("admin/structure/menu/manage/{$this->formState['build_info']['args'][1]['menu_name']}/edit");
    }
  }

}