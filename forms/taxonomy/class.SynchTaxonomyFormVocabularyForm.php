<?php

class SynchTaxonomyFormVocabularyForm extends SynchForm {

  public function getFormId() {
    return "taxonomy_form_vocabulary";
  }

  public function createInstance() {
    return new SynchTaxonomyFormVocabularyFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    if (isset($formState['vocabulary']->vid)) {
      return t('Editing vocabulary !name (#!vid)', array('!name' => $formState['vocabulary']->name, '!vid' => $formState['vocabulary']->vid));
    } else {
      return t('Creating a new vocabulary');
    }
  }

  public function getHelp() {
    return t('Adds, edits or deletes a new vocabulary');
  }

  public function getHelpExceptions() {
    return array(
      t('when adding, if the chosen machine name already exists, an error is shown'),
      t('when editing, if the vocabulary doesn\'t exist locally, an error is shown'),
    );
  }
}

class SynchTaxonomyFormVocabularyFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'taxonomy') . '/taxonomy.admin.inc');
  }

  public function getLocalValues() {
    $values = parent::getLocalValues() + array('vid');
    if (!empty($this->formState['build_info']['args'])) {
      $values[] = 'machine_name';
    }
    return $values;
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    if (!isset($this->info['new_vid'])) {
      //Editing or deleting existing vocabulary: localize vid
      $taxonomy = taxonomy_vocabulary_load($this->source->getLocalId('taxonomy_vocabulary', $this->formState['vocabulary']->vid));
      if (!$taxonomy) {
        //Vocabulary does not exist! Don't submit form as it would result in an error
        drupal_set_message(t('Form #!id not submitted because vocabulary does not exist: \'!name (#!vid).\'', array('!id' => $this->id, '!name' => $this->formState['vocabulary']->name, '!vid' => $this->formState['vocabulary']->vid)), 'warning');
        return false;
      }

      $this->formState['build_info']['args'][0] = $taxonomy;
      $this->formState['vocabulary'] = $taxonomy;
      $this->formState['values']['vid'] = $taxonomy->vid;
      $this->formState['values']['old_machine_name'] = $taxonomy->machine_name;
    }

    return true;
  }

  public function preSave($finalFormState) {
    parent::preSave($finalFormState);

    //Check if existing vocabulary is edited
    if (count($this->formState['build_info']['args']) > 0) {
      //Edited: don't save new vid
    } else {
      $this->info['new_vid'] = $finalFormState['vocabulary']->vid;
    }
  }

  public function ignore($finalFormState) {
    if (!empty($finalFormState['confirm_delete']) && !empty($finalFormState['rebuild'])) {
      //The delete button was pressed and the confirmation form is being built. As nothing is done until the confirmation button is clicked, we can ignore this form.
      return TRUE;
    }
    return FALSE;
  }

  public function prepareFormState(&$form_state) {
    parent::prepareFormState($form_state);
    if (!empty($this->formState['confirm_delete'])) {
      //Prepare for direct deletion
      $form_state['confirm_delete'] = TRUE;
      $form_state['values']['vid'] = $form_state['build_info']['args'][0]->vid;
    }
  }

  public function skip() {
    parent::skip();

    if (isset($this->info['new_vid'])) {
      $this->source->setLocalId('taxonomy_vocabulary', $this->info['new_vid'], NULL);
    }
  }

  public function postExec($formState) {
    parent::postExec($formState);

    //Post process
    if (isset($this->info['new_vid'])) {
      $this->source->setLocalId('taxonomy_vocabulary', $this->info['new_vid'], $formState['vocabulary']->vid);
    }
  }

  public function getAction() {
    if (empty($this->formState['build_info']['args'])) {
      return url('admin/structure/taxonomy/add');
    } else {
      return url("admin/structure/taxonomy/{$this->formState['build_info']['args'][0]->machine_name}/edit");
    }
  }

  public function getCachedFormStateKeys() {
    $keys = parent::getCachedFormStateKeys();
    $keys[] = 'confirm_delete'; //Deletion confirmation form
    return $keys;
  }

}