<?php

class SynchTaxonomyOverviewVocabulariesForm extends SynchForm {

  public function getFormId() {
    return "taxonomy_overview_vocabularies";
  }

  public function createInstance() {
    return new SynchTaxonomyOverviewVocabulariesFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Save order of all taxonomy vocabularies.');
  }

  public function getHelp() {
    return t('Edits the vocabulary order');
  }

  public function getHelpExceptions() {
    return array(
      t('if a vocabulary doesn\'t exist locally, it is ignored and a warning is shown'),
      t('if a vocabulary doesn\'t exist remotely, it will not be modified'),
    );
  }

}

class SynchTaxonomyOverviewVocabulariesFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'taxonomy') . '/taxonomy.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Check all blocks
    $model = SynchModel::getInstance();

    //Localize vids
    $new_form = $this->form;
    foreach ($new_form as $key => $value) {
      if (is_numeric($key)) {
        unset($new_form[$key]);
      }
    }

    foreach ($this->form as $key => $value) {
      if (is_numeric($key)) {
        //Check if block exists
        $voc = taxonomy_vocabulary_load($this->source->getLocalId('taxonomy_vocabulary', $key));
        if (!$voc) {
          //Vocabulary does not exist! Ignore block
          drupal_set_message(t('Form #!id, vocabulary #!vid is ignored because it does not exist', array('!id' => $this->id, '!vid' => $key)), 'warning');
        } else {
          // Add to new form.
          $new_form[$voc->vid] = $this->form[$key];
          $new_form[$voc->vid]['#vocabulary'] = $voc;
        }
      }
    }
    $this->form = $new_form;

    return true;
  }

  public function getAction() {
    return url('admin/structure/taxonomy');
  }

}