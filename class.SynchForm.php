<?php

/**
 * A form type that is synchronized
 */
abstract class SynchForm {

  /**
   * Creates a new form submit object
   */
  public function __construct() {
    $this->source = null;
  }

  /**
   * Returns the form id that is synchronized
   * @return String
   */
  public abstract function getFormId();

  /**
   * Prepares the form for synchronization
   */
  public function prepareForm(&$form, &$formState) {
    $form['#after_build'][] = '_synch_save_form_state';

    //Add description of modification
    $form['synch_description'] = array(
      '#type' => 'textfield',
      '#maxlength' => 99999,
      '#default_value' => $this->getDefaultSynchDescription($formState),
      '#title' => t('Modification description'),
      '#description' => t('Describe the modification that you are making, for synchronisation purposes.'),
      '#weight' => 999999,
    );

    synch_add_form_handler($form, 'submit', 'synch_submit_form');
  }

  /**
   * Submits a form with the specified form state
   * @param Array $formState  The form state (after the form submit)
   */
  public function submitForm($formState) {
    $formId = SynchModel::getFormId($formState);
    if ($formId != $this->getFormId()) throw new Exception("Incorrect form id: " . $formId);

    $submit = $this->createInstance();
    $submit->source = null;
    $submit->formId = $this->getFormId();
    $submit->formState = $formState['__synch_after_build_form_state'];

    //Remove the complete form to save some disk space, as we won't need it anyway.
    unset($submit->formState['complete form']);

    $submit->form = $formState['__synch_built_form'];

    //Set synchronisation description.
    if (!empty($formState['values']['synch_description'])) {
      $desc = $formState['values']['synch_description'];
    }
    elseif (!empty($formState['input']['synch_description'])) {
      //When cloning, adding or deleting a display in the view forms, the synch_description is not added.
      $desc = $formState['input']['synch_description'];
    }
    else {
      $desc = "";
    }
    $submit->description = $desc;

    //Check if form must be ignored.
    if (!$submit->ignore($formState)) {
      $submit->preSave($formState);

      $submit->save();
    }
  }

  /**
   * Returns a new instance
   * @return SynchFormSubmit
   */
  public abstract function createInstance();

  /**
   * Returns the default synchronisation description for a synchronisation of this form
   */
  public function getDefaultSynchDescription($formState) {
    return t("Submission of form with id !form_id", array('!form_id' => SynchModel::getFormId($formState)));
  }

  /**
   * Returns a help text
   */
  public abstract function getHelp();

  /**
   * Returns help about exceptions when submitting this form
   */
  public abstract function getHelpExceptions();

}