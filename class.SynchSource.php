<?php

/**
 * A synchronisation source
 */
class SynchSource {

  /**
   * The name/id that this source is currently stored with in the database. Null iff new.
   */
  private $currentName;

  /**
   * The name of this source
   * @var String
   */
  public $name;

  /**
   * The host of the db of this source
   * @var String
   */
  public $host;

  /**
   * The user of the db of this source; null iff same as this project's main db
   * @var String
   */
  public $user;

  /**
   * The password of the db of this source; null iff same as this project's main db
   * @var String
   */
  public $pass;

  /**
   * The database of this source; null iff same as this project's main db
   * @var String
   */
  public $database;

  /**
   * The table prefix for this source; null iff same as this project's main db
   * @var String
   */
  public $prefix;

  /**
   * The last synchronized form submit id; null iff never synchronized
   * @var int
   */
  public $lastSynchedId;

  /**
   * Creates a new synch source object
   */
  public function __construct() {
    $this->currentName = null;
    $this->lastSynchedId = 0;
  }

  /**
   * Loads a department by a single record fetched using db_query
   */
  public function loadByRecord($record) {
    $this->name = $record->name;
    $this->host = $record->host;
    $this->user = $record->user;
    $this->pass = $record->pass;
    $this->database = $record->database;
    $this->prefix = $record->prefix;
    $this->lastSynchedId = $record->last_synched_id;
    $this->currentName = $this->name;
  }

  /**
   * Returns true iff this source is new (does not yet exist in the database)
   */
  public function isNew() {
    return ($this->currentName == null);
  }

  /**
   * Updates the last synched id
   */
  public function setLastSynchedId($id) {
    $this->lastSynchedId = $id;
    db_update('synch_source')->fields(array(
      'last_synched_id' => $this->lastSynchedId,
    ))
    ->condition('name', $this->currentName)
    ->execute();
  }

  /**
   * Returns the database info
   * @return Array with database connection info
   */
  public function getDbInfo() {
    $tmp = Database::getConnectionInfo('default');
    $db = $tmp['default'];

    //Overwrite platform-specific settings
    $db['host'] = $this->host;
    if ($this->host != null) $db['host'] = $this->host;
    if ($this->user != null) $db['username'] = $this->user;
    if ($this->pass != null) $db['password'] = $this->pass;
    if ($this->database != null) $db['database'] = $this->database;
    if ($this->prefix != null) $db['prefix'] = $this->prefix;
    return $db;
  }

  /**
   * Returns the form submit with the specified id.
   * @param int $id
   * @return SynchFormSubmit
   */
  public function getFormSubmit($id) {
    $source_db = $this->getDbInfo();

    //Temporarily add connection info
    $tempDbName = 'synch_' . $this->currentName;
    Database::addConnectionInfo($tempDbName, 'default', $source_db);

    $conn = Database::getConnection('default', $tempDbName);

    $result = $conn->select('synch_form_submit', 's')
        ->fields('s')
        ->condition('id', $id)
        ->execute();

    $record = $result->fetchObject();
    if (!$record) return NULL;

    $model = SynchModel::getInstance();
    $formId = $record->form_id;
    $synchForm = $model->getSynchForm($formId);
    if (!$synchForm) {
      throw new Exception("Synch Form for form id: '" . $formId . "' does not exist! Is your synch module up to date?");
    }
    $form = $synchForm->createInstance();
    $form->loadByRecord($record);
    $form->source = $this;

    //Delete temporarily added db info
    Database::removeConnection($tempDbName);

    return $form;
  }

  /**
   * Returns all new (not yet synchronized) form submits of this source, correctly ordered
   * @param int $count  Max. number of submits to return
   * @return SynchFormSubmit[]
   */
  public function getNewFormSubmits($count = null) {
    $source_db = $this->getDbInfo();

    //Temporarily add connection info
    $tempDbName = 'synch_' . $this->currentName;
    Database::addConnectionInfo($tempDbName, 'default', $source_db);

    $conn = Database::getConnection('default', $tempDbName);

    $query = $conn->select('synch_form_submit', 's')
        ->fields('s')
        ->orderBy('id');

    //Select all form submit forms
    if ($this->lastSynchedId != null) {
      //Only get form submits since last synch
      $query->condition('id', $this->lastSynchedId, '>');
    }
    if ($count != null) $query->range(0, $count);
    $result = $query->execute();

    //Theme forms
    $forms = array();
    $model = SynchModel::getInstance();
    foreach ($result as $record) {
      $formId = $record->form_id;
      $synchForm = $model->getSynchForm($formId);
      if (!$synchForm) {
        throw new Exception("Synch Form for form id: '" . $formId . "' does not exist! Is your synch module up to date?");
      }
      $form = $synchForm->createInstance();
      $form->loadByRecord($record);
      $form->source = $this;
      $forms[] = $form;
    }

    //Delete temporarily added db info
    Database::removeConnection($tempDbName);

    return $forms;
  }

  /**
   * Returns the local id for the specified object
   * @param String $type  The object type
   * @param String $id The remote object id
   * @return String, or null iff the specified object has no equal object locally and must be skipped
   */
  public function getLocalId($type, $id) {
   $result = db_select('synch_remote_key', 's')
      ->fields('s', array('local_id'))
      ->condition('s.{source_name}', $this->currentName)
      ->condition('s.{type}', $type)
      ->condition('s.id', $id)
      ->execute();
    $record = $result->fetchObject();
    if (!$record) {
      //Assume that the id is the same
      return $id;
    }
    return (int) $record->local_id;
  }

  /**
   * Sets the local id for the specified object
   * @param String $type  The object type
   * @param String $id The remote object id
   * @param String $localId  The local object id, or null iff the specified object has no equal object locally and must be skipped
   */
  function setLocalId($type, $id, $localId) {
    //Delete 'old' key iff it exists
    db_delete('synch_remote_key')
      ->condition('source_name', $this->currentName)
      ->condition('{type}', $type)
      ->condition('id', $id)
      ->execute();

    //Translate
    $fields = array(
      'source_name' => $this->currentName,
      'type' => $type,
      'id' => $id,
      'local_id' => $localId,
    );

    //Insert record with form data in it
    db_insert('synch_remote_key')
      ->fields($fields)
      ->execute();
  }

  /**
   * Saves this synch source to the database
   */
  public function save() {
    $fields = array(
      'name' => $this->name,
      'host' => $this->host,
      'user' => $this->user,
      'pass' => $this->pass,
      '`database`' => $this->database,
      'prefix' => $this->prefix,
    );

    if ($this->currentName == null) {
      //Insert
      db_insert('synch_source')
        ->fields($fields)
        ->execute();
    } else {
      //Update
      db_update('synch_source') // Table name no longer needs {}
        ->fields($fields)
        ->condition('name', $this->currentName, '=')
        ->execute();

      if ($this->name != $this->currentName) {
        db_update('synch_remote_key') // Table name no longer needs {}
          ->fields(array('name' => $this->name))
          ->condition('name', $this->currentName, '=')
          ->execute();
      }
    }

    $this->currentName = $this->name;
  }

  /**
   * Deletes this synch source from the database
   */
  public function delete() {
    db_delete('synch_source')
      ->condition('name', $this->currentName, '=')
      ->execute();

    db_delete('synch_remote_key')
      ->condition('source_name', $this->currentName, '=')
      ->execute();

    $this->currentName = null;
  }
}