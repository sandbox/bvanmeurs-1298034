/*
 * Javascript that opens up the synched form when clicking on the submit button
 */
(function($) {

$(document).ready(function() {
  
  /**
   * Converts the parameters (a recursive array) to several value form elements
   * @return array 
   */
  var get_form_elements = function(params, parent_names) {
    if (!parent_names) parent_names = new Array();
    
    var inputs = new Array();
    for (name in params) {
      var value = params[name];
      
      //Add to parent names
      var param_parent_names = parent_names.slice();
      param_parent_names.push(name);
      
      if (value instanceof Object) {
        inputs = inputs.concat(get_form_elements(value, param_parent_names));
      } else {
        //Create post path
        path = param_parent_names[0];
        for (var i = 1; i < param_parent_names.length; i++) {
          path += '[' + param_parent_names[i] + ']';
        }
        
        if (path == 'submit') {
          //This field would override the javascript/jquery submit function, so we ignore it.
        } else if (value != null) {
          var input = document.createElement('input');
          input.setAttribute('type', 'hidden');
          input.setAttribute('name', path);
          input.setAttribute('value', value);
          
          inputs.push(input);
        }
      }
    }
    
    return inputs;
  }

  $('#edit-open').click(function() {
    var post_json = $('input[name="open_post_data"]').val();
    var data = $.parseJSON(post_json);
    var form = $('<form name="synch_post_form" "accept-charset="UTF-8" method="post" />');
    form.attr('action', data.action);
    form.css('display', 'none');
    
    // This makes sure that the form is not directly submitted, and allows the user to make modifications.
    var form_initial = $('<input type="hidden" name="synch_initial_submit" value="1" />');
    form.append(form_initial);
    
    // The following must be done, because an input element named 'submit' may cause a problem..
    inputs = get_form_elements(data.posts);
    for (var i = 0; i < inputs.length; i++) {
      form.append(inputs[i]);
    }
    
    $(document.body).append(form);

    form.submit();
    
    return false;
  });
});

})(jQuery);
