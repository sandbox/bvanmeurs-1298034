<?php

class SynchSystemModulesForm extends SynchForm {

  public function getFormId() {
    return "system_modules";
  }

  public function createInstance() {
    return new SynchSystemModulesFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Enabling/disabling modules');
  }

  public function getHelp() {
    return t('Enables or disables modules');
  }

  public function getHelpExceptions() {
    return array(
      t('if a module does not exist locally, it will be ignored and a warning is shown'),
      t('if a module does not exist remotely, it will not be modified'),
      t('this form is confirmed automatically'),
    );
  }

}

class SynchSystemModulesFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'system') . '/system.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Form must be confirmed automatically!
    $this->formState['input']['confirm'] = 1;
    $this->formState['values']['confirm'] = 1;

    return true;
  }

  public function getAction() {
    return url('admin/modules');
  }

}