<?php

class SynchUserAdminSettingsForm extends SynchForm {

  public function getFormId() {
    return "user_admin_settings";
  }

  public function createInstance() {
    return new SynchUserAdminSettingsFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    //Configure node type
    return t('Configuring user settings');
  }

  public function getHelp() {
    return t('Change the user account settings');
  }

  public function getHelpExceptions() {
    return array(
      t('if the user_admin_role field is set to a role that does not exist locally, an error is shown'),
    );
  }
}

class SynchUserAdminSettingsFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'user') . '/user.admin.inc');
  }

  public function prepareFormForSynch($local_form) {
    // Localize user_admin_role options.
    $user_admin_role =& $this->form['admin_role']['user_admin_role'];

    $new_user_admin_roles = array();
    foreach ($user_admin_role['#options'] as $rid => $name) {
      if (!$rid) {
        //Disabled option
        $new_user_admin_roles[$rid] = $name;
      }

      $local_rid = $this->source->getLocalId('role', $rid);
      $local_role = user_role_load($local_rid);
      if ($local_role) {
        $new_user_admin_roles[$local_rid] = $local_role->name;
      }
    }

    $user_admin_role['#options'] = $new_user_admin_roles;

    //Localize value
    if ($user_admin_role['#value']) {
      $user_admin_role['#value'] = $this->source->getLocalId('role', $user_admin_role['#value']);
    }

    unset($user_admin_role);
  }

  public function getAction() {
    return url("admin/config/people/accounts");
  }

}