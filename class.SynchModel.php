<?php

require_once("class.SynchForm.php");
require_once("class.SynchFormSubmit.php");
require_once("class.SynchSource.php");
require_once("class.SynchGenericForm.php");


/**
 * Synchronization datamodel
 */
class SynchModel {

  /**
   * @var Model
   */
  private static $instance = null;

  /**
   * Returns the SynchModel
   * @return SynchModel
   */
  public static function getInstance() {
    if (SynchModel::$instance == null) {
      SynchModel::$instance = new SynchModel();
    }
    return SynchModel::$instance;
  }

  /**
   * The available synch forms
   * @var SynchForm[]
   */
  private $forms;

  /**
   * Creates a new form submit object
   */
  private function __construct() {
    $this->forms = array();

    $path = drupal_get_path('module', 'synch') . "/forms";

    //Scan for synch forms and add them
    $this->scandirSynchForms($path);
  }

  /**
   * Get all synchronization sources
   * @return SynchSource[]
   */
  public function getSources() {
    $result = db_select('synch_source', 's')
      ->fields('s')
      ->orderBy('name')
      ->execute();
    $sources = array();
    foreach($result as $record) {
      $source = new SynchSource();
      $source->loadByRecord($record);
      $sources[] = $source;
    }
    return $sources;
  }

  /**
   * Get the specified source, or null iff it doesn't exist
   * @param String $name
   * @return SynchSource
   */
  public function getSource($name) {
    $result = db_select('synch_source', 's')
      ->fields('s')
      ->condition('name', $name)
      ->execute();
    $record = $result->fetchObject();
    if (!$record) return null;

    $source = new SynchSource();
    $source->loadByRecord($record);
    return $source;
  }

  /**
   * Submits a form with the specified form state
   * @param Array $formState  The form state (after the form submit)
   */
  public function submitForm($formState) {
    $formId = SynchModel::getFormId($formState);
    $synchForm = $this->getSynchForm($formId);
    if ($synchForm == null) throw new Exception("Form id not supported: " . $formId);
    $synchForm->submitForm($formState);
  }

  /**
   * Adds a new form id handler to the synchronization model
   */
  public function addSynchForm(SynchForm $synchForm) {
    $this->forms[$synchForm->getFormId()] = $synchForm;
  }

  /**
   * Returns the SynchForm for the specified $formId, or null iff none exists
   * @return SynchForm
   */
  public function getSynchForm($formId) {
    if (array_key_exists($formId, $this->forms)) {
      return $this->forms[$formId];
    } else {
      return null;
    }
  }

  /**
   * Returns true iff there is a synch form for the specified id
   * @return bool
   */
  public function hasSynchForm($formId) {
    return array_key_exists($formId, $this->forms);
  }

  /**
   * Adds all synch form objects that exist within the /forms directory
   * @return SynchForm[]
   */
  private function scandirSynchForms($path) {
    $contents = scandir($path);
    foreach ($contents as $item) {
      if ($item == "." || $item == "..") continue;

      $itemPath = $path . "/" . $item;
      if (is_dir($itemPath)) {
        $this->scandirSynchForms($itemPath);
      } else {
        $matches = array();
        if (preg_match("/^class\.(Synch.+Form)\.php$/", $item, $matches)) {
          $name = $matches[1];
          require_once($itemPath);
          $synchForm = new $name();
          $this->addSynchForm($synchForm);
        } else if ($item == "generic.info") {
          //Import generic forms
          $handle = fopen($itemPath, "r");
          while($line = fgets($handle)) {
            $matches = array(); //Strips ending newline characters from $line
            if (preg_match("/^(\w+)$/", $line, $matches)) {
              //Add generic form
              $this->addSynchForm(new SynchGenericForm($matches[1]));
            } else {
              throw new Exception("Incorrect form id found in '" . $itemPath . "': '" . $line . "'");
            }
          }
          fclose($handle);
        }
      }
    }
  }

  /**
   * Returns the form id of the form for the specified form state
   */
  public static function getFormId($formState) {
    if (isset($formState['values']['form_id'])) {
      return $formState['values']['form_id'];
    } else if (isset($formState['complete form']['form_id']['#value'])) {
      return $formState['complete form']['form_id']['#value'];
    } else {
      throw new Exception("Can't find form id in form state!");
    }
  }
}
