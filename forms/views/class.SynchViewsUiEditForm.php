<?php

class SynchViewsUiEditForm extends SynchForm {

  public function getFormId() {
    return "views_ui_edit_form";
  }

  public function createInstance() {
    return new SynchViewsUiEditFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    if (!$formState['view']->vid) {
      return t('Saving a new view');
    } else {
      return t('Saving an existing view');
    }
  }

  public function getHelp() {
    return t('Edit a view');
  }

  public function getHelpExceptions() {
    return array(
      t('if the view does not exist locally, it will be added automatically'),
      t('when synchronizing an existing view, all of its displays will be replaced!'),
    );
  }
}

class SynchViewsUiEditFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(
      drupal_get_path('module', 'ctools') . '/includes/object-cache.inc',
      drupal_get_path('module', 'views') . '/includes/admin.inc',
    );
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Get local version
    if (!$this->localizeView($this->formState['build_info']['args'][0])) {
      drupal_set_message(t('Form #!id not submitted because view does not exist: \'!view_name.\'', array('!id' => $this->id, '!view_name' => $view->name)), 'warning');
      return false;
    }

    return true;
  }

  /**
   * Localize the specified view object
   * @param view $view
   * @return bool
   *   TRUE if the view could be properly localized
   */
  private function localizeView(&$view) {
    $viewName = $this->source->getLocalId('view', $view->name);
    $local_view = views_get_view($viewName);

    if ($local_view) {
      //Replace vids
      $view->vid = $local_view->vid;
      foreach ($view->display as &$display) {
        $display->vid = $local_view->vid;
      }
      unset($display);

      return TRUE;
    }

    return FALSE;
  }


  public function prepareFormState(&$form_state) {
    parent::prepareFormState($form_state);
    if (!empty($this->formState['view'])) {
      //Add the edited (and localized) view to the form state
      $view = $this->formState['view'];
      $this->localizeView($view);
      $form_state['view'] = $view;
    }
  }

  public function postExec($formState) {
    parent::postExec($formState);

		//Clear cache: blocks must be discovered
		cache_clear_all();
  }

  public function getAction() {
    return url("admin/structure/views/view/{$this->formState['build_info']['args'][0]->name}/edit");
  }

}