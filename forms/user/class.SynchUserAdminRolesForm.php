<?php

class SynchUserAdminRolesForm extends SynchForm {

  public function getFormId() {
    return "user_admin_roles";
  }

  public function createInstance() {
    return new SynchUserAdminRolesFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    //Configure node type
    return t('Configuring user roles');
  }

  public function getHelp() {
    return t('Change the order of the roles or add a new role');
  }

  public function getHelpExceptions() {
    return array(
      t('if the role does not exist locally, it is ignored and a warning is shown'),
      t('if the role does not exist remotely, it is not modified'),
    );
  }

}

class SynchUserAdminRolesFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'user') . '/user.admin.inc');
  }

  public function preSave($finalFormState) {
    parent::preSave($finalFormState);

    if ($this->isAddRoleForm()) {
      //Save new role id
      $this->info['new_role_id'] = $this->getRoleId($finalFormState['values']['name']);
    }
  }

  /**
   * Returns true iff this is an add role form.
   */
  public function isAddRoleForm() {
    return $this->formState['triggering_element']['#value'] == t('Add role');
  }

  /**
   * Returns the role id for the specified role name
   * @return int
   */
  private function getRoleId($roleName) {
    $role = user_role_load_by_name($roleName);
    if (!$role) {
      throw new Exception('Role should have been added but does not exist in database: ' . $roleName);
    }
    return $role->rid;
  }

  public function skip() {
    parent::skip();

    if ($this->isAddRoleForm()) {
      //Set local role to 'no equivalent'
      $roleName = $this->formState['values']['name'];
      $this->source->setLocalId('role', $this->info['new_role_id'], NULL);
    }
  }

  public function prepareFormForSynch($local_form) {
    $new_roles = array();
    foreach ($this->form['roles'] as $rid => $values) {
      if (is_numeric($rid)) {
        $local_rid = $this->source->getLocalId('role', $rid);
        $local_role = user_role_load($local_rid);
        if ($local_role) {
          $new_roles[$local_rid] = $this->form['roles'][$rid];
          $new_roles[$local_rid]['#role'] = $local_role;
        }
        unset($this->form['roles'][$rid]);
      }
    }

    foreach ($new_roles as $rid => $values) {
      $this->form['roles'][$rid] = $values;
    }
  }

  public function postExec($formState) {
    parent::postExec($formState);

    if ($this->isAddRoleForm()) {
      //Set local role id
      $roleName = $formState['values']['name'];
      $this->source->setLocalId('role', $this->info['new_role_id'], $this->getRoleId($roleName));
    }
  }

  public function getAction() {
    return url('admin/people/permissions/roles');
  }

}