<?php

class SynchUserAdminPermissionsForm extends SynchForm {

  public function getFormId() {
    return "user_admin_permissions";
  }

  public function createInstance() {
    return new SynchUserAdminPermissionsFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    //Configure node type
    return t('Setting role permissions');
  }

  public function getHelp() {
    return t('Sets permissions for all roles');
  }

  public function getHelpExceptions() {
    return array(
      t('if a role does not exist locally, it is ignored and a warning is shown'),
      t('if a role does not exist remotely, it is not modified'),
      t('if a permission does not exist locally, it is ignored'),
      t('if a permission does not exist remotely, the current settings are loaded and used'),
    );
  }

}

class SynchUserAdminPermissionsFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'user') . '/user.admin.inc');
  }

  public function preSave($finalFormState) {
    parent::preSave($finalFormState);

    $this->info['permissions'] = array_keys(module_invoke_all('permission'));
  }

  private function getDelFsIndices() {
    $indices = parent::getDelFsIndices();
    unset($indices['input']);
    return $indices;
  }

  public function prepareFormForSynch($local_form) {
    $new_role_names = array();
    $new_checkboxes = array();
    foreach ($this->form['role_names'] as $rid => $values) {
      if (is_numeric($rid)) {
        $local_rid = $this->source->getLocalId('role', $rid);
        if ($local_rid && user_role_load($local_rid)) {
          $new_role_names[$local_rid] = $this->form['role_names'][$rid];
          $new_checkboxes[$local_rid] = $this->form['checkboxes'][$rid];
        }
        unset($this->form['role_names'][$rid]);
        unset($this->form['checkboxes'][$rid]);
      }
    }

    foreach ($new_role_names as $rid => $values) {
      $this->form['role_names'][$rid] = $values;
    }

    foreach ($new_checkboxes as $rid => $values) {
      $this->form['checkboxes'][$rid] = $values;
    }
  }

  public function getAction() {
    return url('admin/people/permissions');
  }

}