<?php

class SynchViewsUiAddForm extends SynchForm {

  public function getFormId() {
    return "views_ui_add_form";
  }

  public function createInstance() {
    return new SynchViewsUiAddFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Adding a view');
  }

  public function getHelp() {
    return t('Add a view');
  }

  public function getHelpExceptions() {
    return array(
    );
  }
}

class SynchViewsUiAddFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(
      drupal_get_path('module', 'ctools') . '/includes/object-cache.inc',
      drupal_get_path('module', 'views') . '/includes/admin.inc',
    );
  }

  public function skip() {
    parent::skip();
    $this->source->setLocalId('view', $this->formState['values']['name'], NULL);
  }

  public function postExec($formState) {
    parent::postExec($formState);

		//Clear cache: blocks must be discovered
		cache_clear_all();
  }

  public function getAction() {
    return url("admin/structure/views/add");
  }

}