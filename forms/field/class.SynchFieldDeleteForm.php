<?php

class SynchFieldDeleteForm extends SynchForm {

  public function getFormId() {
    return "field_ui_field_delete_form";
  }

  public function createInstance() {
    return new SynchFieldDeleteFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    $field = $formState['build_info']['args'][0];
    return t('Delete field instance \'!label\' from \'!entity_type,!bundle\'', array('!label' => $field['label'], '!entity_type' => $field['entity_type'], '!bundle' => $field['bundle']));
  }

  public function getHelp() {
    return t('Deletes a field instance');
  }

  public function getHelpExceptions() {
    return array(
      t('if the field instance doesn\'t exist locally, an error is shown'),
    );
  }

}

require_once("class.SynchFieldActionFormSubmit.php");
class SynchFieldDeleteFormSubmit extends SynchFieldActionFormSubmit {

  public function getActionType() {
    return "delete";
  }

}