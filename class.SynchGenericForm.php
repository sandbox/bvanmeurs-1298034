<?php

/**
 * A generic form that has no additional functionality
 */
class SynchGenericForm extends SynchForm {
  
  private $formId;
  
  public function __construct($formId) {
    parent::__construct();
    $this->formId = $formId;
  }
  public function getFormId() {
    return $this->formId;
  }
  
  public function createInstance() {
    return new SynchGenericFormSubmit();
  }
  
  public function getDefaultSynchDescription($formState) {
    return t('Submitting form');
  }
  
  public function getHelp() {
    return t('Submit the specified form.');
  }
  
  public function getHelpExceptions() {
    return array(
      t('this form was handled as a generic form'),
    );
  }

}

class SynchGenericFormSubmit extends SynchFormSubmit {

}