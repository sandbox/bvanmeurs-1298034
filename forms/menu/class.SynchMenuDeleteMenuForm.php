<?php

class SynchMenuDeleteMenuForm extends SynchForm {

  public function getFormId() {
    return "menu_delete_menu_confirm";
  }

  public function createInstance() {
    return new SynchMenuDeleteMenuFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Deleting menu !menu_name', array('!menu_name' => $formState['build_info']['args'][0]['menu_name']));
  }

  public function getHelp() {
    return t('Deletes a menu');
  }

  public function getHelpExceptions() {
    return array(
      t('if the menu doesn\'t exist locally, an error is shown'),
    );
  }

}

class SynchMenuDeleteMenuFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'menu') . '/menu.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Check and reset build info
    $menuName = $this->source->getLocalId('menu', $this->formState['build_info']['args'][0]['menu_name']);
    $menu = menu_load($menuName);
    if (!$menu) {
      drupal_set_message(t('Form #!id not submitted because menu does not exist: \'!menu_name\'', array('!id' => $this->id, '!menu_name' => $menuName)), 'warning');
      return false;
    }
    $this->formState['build_info']['args'][0] = $menu;

    return true;
  }

  public function getAction() {
    return url("admin/structure/menu/manage/{$this->formState['build_info']['args'][0]['menu_name']}/delete");
  }

}