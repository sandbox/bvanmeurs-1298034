<?php

class SynchNodeTypeDeleteForm extends SynchForm {

  public function getFormId() {
    return "node_type_delete_confirm";
  }

  public function createInstance() {
    return new SynchNodeTypeDeleteFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    $typeObj = $formState['build_info']['args'][0];
    return t('Deleting node type \'!id\' with name \'!name\'', array('!id' => $typeObj->type, '!name' => $typeObj->name));
  }

  public function getHelp() {
    return t('Deletes a node type');
  }

  public function getHelpExceptions() {
    return array(
      t('if the node type does not exist locally, an error is shown'),
    );
  }

}

class SynchNodeTypeDeleteFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'node') . '/content_types.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Check if node type exists
    $type = $this->source->getLocalId('node_type', $this->formState['build_info']['args'][0]->type);

    //Check if block exists
    $types = node_type_get_types();
    if (!array_key_exists($type, $types)) {
      //Node type does not exist! Don't submit form as it would result in an error
      drupal_set_message(t('Can\'t submit form #!id not submitted because node type does not exist: \'!type\'', array('!id' => $this->id, '!type' => $type)), 'warning');
      return false;
    }

    $this->formState['build_info']['args'][0] = $types[$type];

    return true;
  }

  public function getAction() {
    return url("admin/structure/types/manage/{$this->formState['build_info']['args'][0]->type}/delete");
  }

}