<?php

/**
 * A form submit
 */
class SynchFormSubmit {

  /**
   * The id of this form submit
   * @var int
   */
  public $id;

  /**
   * The description
   * @var String
   */
  public $description;

  /**
   * The form id of the form that this submit describes
   * @var String
   */
  public $formId;

  /**
   * The intial form state, that contains the build_info arguments and storage that can be used to build the (original) form.
   * @var Array
   */
  public $formState;

  /**
   * The fully built form structure, filled with the user input values, but not validated or submitted (i.e. drupal_build_form with process_input set to FALSE).
   * @var Array
   */
  public $form;

  /**
   * The created timestamp
   * @var int
   */
  public $created;

  /**
   * The source of this synch form submit; null iff local
   * @var SynchSource
   */
  public $source;

  /**
   * Additional information for the synchronization
   */
  public $info;

  /**
   * The merged form info; cached.
   * @var array
   */
  private $mergedForm;

  /**
   * Creates a new form submit object
   */
  public function __construct() {
    $this->source = null;
    $this->id = null;
    $this->info = array();
    $this->mergedForm = NULL;
  }

  /**
   * Loads a department by a single record fetched using db_query
   */
  public function loadByRecord($record) {
    $this->id = (int) $record->id;
    $this->description = $record->description;
    $this->formId = $record->form_id;
    $this->formState = unserialize($record->form_state);
    $this->form = unserialize($record->form);
    $this->created = (int) $record->created;
    $this->info = unserialize($record->info);
    $this->mergedForm = NULL;
  }

  /**
   * Saves this form submit to the local database
   * @pre $this->source is null (form submit is local)
   */
  public function save() {
    if ($this->id != null) throw new Exception("Form submit should be new");
    if ($this->source != null) throw new Exception("Form submit should be local");

    //Save to database
    $fields = array(
      'form_id' => $this->formId,
      'form_state' => serialize($this->formState),
      'form' => serialize($this->form),
      'created' => time(),
      'description' => $this->description,
      'info' => serialize($this->info),
    );

    //Insert record with form data in it
    db_insert('synch_form_submit')
      ->fields($fields)
      ->execute();

  }

  /**
   * Preprocesses this form submit before executing it, and returns true iff it can be executed
   * @pre The form state is just before validation
   */
  public function preExec() {
    $this->requireIncludes();

    return true;
  }

  /**
   * Called just before saving this form submit (on the platform that the form was just executed)
   * @param $finalFormState The form state after the complete submission; this may contain additional information about added objects and object ids
   */
  public function preSave($finalFormState) {
    $this->requireIncludes();

    //Remove unnecessary fields from form state
    foreach ($this->getDelFsIndices() as $index) {
      unset($this->formState[$index]);
    }
  }

  /**
   * Returns an array of form state array indices that will be removed before saving
   */
  private function getDelFsIndices() {
    return array('complete form');
  }

  /**
   * Requires the specified includes
   */
  public function requireIncludes() {
    $includes = $this->getRequiredIncludes();
    foreach ($includes as $include) {
      require_once($include);
    }
  }

  /**
   * Returns true if this form must be ignored, and not synchronized. Called before saving.
   *
   * @param array $finalFormState
   *   The form state after submission.
   *
   * @return bool
   */
  public function ignore($finalFormState) {
    return FALSE;
  }

  /**
   * Requires the specified includes
   */
  public function getRequiredIncludes() {
    return array();
  }

  /**
   * Prepares the form state for the local form submit, adding storage when needed.
   */
  public function prepareFormState(&$form_state) {
    // Default: do nothing.
  }

  /**
   * Prepare $this->form for the synchronisation, according to the produced local form.
   *
   * @param array $local_form
   */
  public function prepareFormForSynch($local_form) {
    // Default: do nothing.
  }

  /**
   * Returns the form and form state of the merged local and remote form
   * @pre The preExec hook has been called
   */
  public function getMergedForm() {
    if ($this->mergedForm === NULL) {
      $this->requireIncludes();

      // Prevent strange form ids.
      drupal_static_reset('drupal_html_id');

      //Create a blanco form state
      $form_state = array();

      //Copy the build info (necessary to build the correct form)
      $form_state['build_info'] = $this->formState['build_info'];

      //Set the form submit information
      $form_state['synch_form_submit_source'] = $this->source->name;
      $form_state['synch_form_submit_id'] = $this->id;

      //We make sure that the initial form state is used by form caching.
      $form_state['no_cache'] = FALSE;
      $form_state['cache'] = TRUE;

      //We don't want to submit this form, but just cache it.
      $form_state['rebuild'] = FALSE;

      //For extra security..
      $form_state['submitted'] = FALSE;

      //Prepare the form state.
      $this->prepareFormState($form_state);

      //Create a fully built local form for the specified form state
      $local_form = drupal_build_form($this->formId, $form_state);

      if ($local_form['#action'] == request_uri()) {
        // Use the form action provided in the known form.
        $form_build_id = $local_form['form_build_id']['#value'];
        $cached = cache_get('form_' . $form_build_id, 'cache_form');

        $cached->data['#action'] = $this->form['#action'];
        cache_set('form_' . $form_build_id, $cached->data, 'cache_form', REQUEST_TIME + 21600);
      }

      //Clear eventual validation errors, but remember them to re-set them later.
      $existing_form_errors = drupal_static('form_set_error', array());

      form_clear_error();

      //Set the inputs in the form state, so that they are appened to
      $form_state['input'] = $this->formState['input'];

      //Reset the form errors
      $temp = &drupal_static('form_set_error');
      $temp = $existing_form_errors;
      unset($temp);

      //Set the overridden form values
      $warnings = $this->copyRemoteValues($form_state, $local_form, $this->form);

      //Get the inputs
      $input = $form_state['input'];

      //Set the form build id
      $input['form_build_id'] = $local_form['form_build_id']['#value'];

      $this->mergedForm = array(
        'input' => $input,
        'build_info' => $form_state['build_info'],
        'warnings' => $warnings,
      );
    }

    return $this->mergedForm;
  }

  /**
   * Returns the action 'url' where this form submit could be found $form['#action'].
   * @pre The preExec hook has been called
   * @return string
   *   The action url.
   */
  public function getAction() {
    return NULL;
  }

  /**
   * Submits this form to this Drupal installation. Note that this is not the preferred way of form submission, as it may cause problems. Use a 'real' form submission instead, unless there is no other way.
   * @param bool $go_to_next_submission
   *   If true, the user will be automatically redirected to the next form submission. If false, he will get the rebuilt form.
   * @pre The preExec hook has been called
   * @post The postExec hook has been called if there are no errors
   * @return Array  eventual errors that occurred while saving (key = field name, value = error)
   */
  public function submit($go_to_next_submission = TRUE) {
    try {
      //Removes all previous errors
      form_clear_error();
      $info = $this->getMergedForm();

      //drupal_get_form simulation, with the POSTed variables

      //Copy the args
      $form_state['build_info'] = $info['build_info'];

      //Copy the inputs
      $form_state['input'] = $info['input'];

      $form_state['programmed'] = TRUE;
      $form_state['submitted'] = TRUE;
      $form_state['must_validate'] = TRUE;

      if ($go_to_next_submission) {
        $form_state['synch_go_to_next_submission'] = TRUE;
      }

      //For the rest, the cache will be used, which contains both the correct built form and the ready form state
      form_clear_error();

      drupal_build_form($this->form['#form_id'], $form_state);

      //Error handling
      $errors = form_get_errors();
      if (!$errors) $errors = array();

      if (count($errors) == 0) {
        //Post-process the form, like setting local ids
        $this->postExec($form_state);
      }

      return $errors;
    } catch(Exception $e) {
      return array('Exception while submitting form: ' . $e);
    }
  }

  /**
   * Returns the element names of the local values, which should not be copied in the copyRemoteValues function.
   */
  public function getLocalValues() {
    return array('form_build_id', 'form_token');
  }

  /**
   * Overrides the local form values with the remote form values where possible.
   *
   * @param Array $form_state
   *   The local form state. Overridden values will also be set here.
   * @param Array $element
   *   The current element
   * @param Array $remote_element
   *   The identical element in the remote form, or NULL if there is none.
   * @return Array
   *   An array with warning messages (assoc. array with 'element_name' and 'description') that occur when copying the remote values.
   */
  private function copyRemoteValues(&$form_state, &$element, $remote_element) {
    $warnings = array();

    //Taken from form.inc
    $process_input = empty($element['#disabled']) && (((!isset($element['#access']) || $element['#access'])));

    if (!empty($element['#name']) && in_array($element['#name'], $this->getLocalValues())) {
      $process_input = FALSE;
    }

    if (!empty($element['#input'])) {
      if (array_key_exists('#value', $element)) {
        $value = $element['#value'];
      }
      if ($process_input && ($remote_element !== NULL)) {
        if (array_key_exists('#value', $remote_element)) {
          //Override value
          $value = $remote_element['#value'];
        }

        //Check options: combine values
        if ($element['#type'] == "radios") {
          // Radios: check if value is correct.
          $local_options = $this->getElementOptions($element);
          if (!array_key_exists($value, $local_options)) {
            // Remote value does not exist as local option.
            $warnings[] = array(
              'element_name' => $element['#name'],
              'description' => t("The option(s) '!value' does not exist in the local form but is enabled in the remote form. It is ignored, and the local value '!local_value' was set.", array('!value' => $value, '!local_value' => $element['#value'])),
            );

            $value = $element['#value'];
          }
        } elseif ($element['#type'] == "checkboxes") {
          // Checkboxes: combine values.
          $local_values = (is_array($element['#value']) ? $element['#value'] : array($element['#value']));
          $remote_values = (is_array($remote_element['#value']) ? $remote_element['#value'] : array( $remote_element['#value']));
          $combined_values = array();
          $local_options = $this->getElementOptions($element);
          $remote_options = $this->getElementOptions($remote_element);
          foreach ($local_options as $key => $v) {
            if (!array_key_exists($key, $remote_options)) {
              // Local option does not exist remotely: keep current value.
              if (in_array($key, $local_values)) {
                $combined_values['' . $key] = TRUE;
              }
            }
            elseif (in_array($key, $remote_values)) {
              // Synchronize.
              $combined_values['' . $key] = TRUE;
            }
          }

          // Add a warning for all option keys that are enabled in the local element but not in the remote element.
          $keys = array_diff($local_values, array_keys($remote_options));
          if (count($keys) > 0) {
            $keys_descs = array();
            foreach ($keys as $key) {
              if (array_key_exists($key, $local_options)) {
                $keys_descs[] = "'{$local_options[$key]}' ({$key})";
              } else {
                $keys_descs[] = "unspecified ({$key})";
              }
            }
            $warnings[] = array(
              'element_name' => $element['#name'],
              'description' => t("The options '!keys' are enabled locally but do not exist as options in the remote form. They are automatically enabled.", array('!keys' => implode(', ', $keys_descs))),
            );
          }

          // Add a warning for all option keys that are enabled in the remote element but not in the local element.
          $keys = array_diff($remote_values, array_keys($local_options));
          if (count($keys) > 0) {
            $keys_descs = array();
            foreach ($keys as $key) {
              $keys_descs[] = "'{$remote_options[$key]}' ({$key})";
            }
            $warnings[] = array(
              'element_name' => $element['#name'],
              'description' => t("The option(s) '!keys' do not exist in the local form but are enabled in the remote form. They are ignored.", array('!keys' => implode(', ', $keys_descs))),
            );
          }

          $value = drupal_map_assoc(array_keys($combined_values));
        } elseif (in_array($element['#type'], array('select'))) {
          $local_options = $this->getElementOptions($remote_element);
          if (!array_key_exists($value, $local_options)) {
            //Invalid option: keep the currently set value
            $warnings[] = array(
              'element_name' => $element['#name'],
              'description' => t("The remote value '!value' is not a selectable option in the local form. The local value '!local_value' is used instead.", array('!value' => $value, '!local_value' => $element['#value'])),
            );

            $value = $element['#value'];
          }
        }
      }

      //General INPUT value corrections
      if (in_array($element['#type'], array('checkbox')) && !$value) {
        //Explicitly set checkbox to NULL instead of 0 (prevent illegal choice error)
        $value = NULL;
      }

      if (array_key_exists('#empty_value', $element)) {
        //In case of an empty value, use the empty_value
        if (!$value) {
          $value = $element['#empty_value'];
        }
      }

      if ($value !== NULL) {
        //Add to input iff the element is not an internal (value) element
        if ($element['#type'] != 'value') {
          drupal_array_set_nested_value($form_state['input'], $element['#parents'], $value, TRUE, TRUE);
        }
      } else {
        // Explicitly unset the value in both the input and the values
        $this->drupalArrayClearNestedValue($form_state['input'], $element['#parents']);
      }
      $element['#value'] = $value;
    }

    //Expanded child checkboxes do not need to be visited; we can get everything using the values.
    if (empty($element['#type']) || (!in_array($element['#type'], array('checkboxes', 'radios')))) {
      $local_children = element_children($element);
      if ($remote_element !== NULL) {
        $remote_children = array_flip(element_children($remote_element));
      } else {
        $remote_children = array();
      }

      foreach ($local_children as $key) {
        $sub_warnings = $this->copyRemoteValues(
          $form_state,
          $element[$key],
          array_key_exists($key, $remote_children) ? $remote_element[$key] : NULL
        );
        $warnings = array_merge($warnings, $sub_warnings);
      }
    }

    return $warnings;
  }

  /**
   * Returns an associative array with all the element options. When the options consists of option groups, these options are combined.
   * @param array $element
   * @return array
   */
  private function getElementOptions($element) {
    $combined_options = array();

    foreach ($element['#options'] as $key => $value) {
      if (is_array($value)) {
        $combined_options = array_merge($combined_options, $value);
      } else {
        $combined_options[$key] = $value;
      }
    }

    return $combined_options;
  }

  /**
   * Unsets the specified element in a values array.
   */
  private function drupalArrayClearNestedValue(array &$array, array $parents) {
    $ref = &$array;

    //Remove the last parent
    $last_parent = array_pop($parents);
    foreach ($parents as $parent) {
      // PHP auto-creates container arrays and NULL entries without error if $ref
      // is NULL, but throws an error if $ref is set, but not an array.
      if (isset($ref) && !is_array($ref)) {
        $ref = array();
      }
      $ref = &$ref[$parent];
    }

    unset($ref[$last_parent]);
  }

  /**
   * Called when this form submit was skipped. Override when entities would have been added (use setLocalId($type, $id, NULL) to make sure no confusion will exists with local objects later)
   */
  public function skip() {
  }

  /**
   * Postprocess this form submit after submitting it
   *
   * @param array $formState
   *   The local form state result
   *
   * @pre The submit hook has been called
   * @pre The form state is after the complete submission
   */
  public function postExec($formState) {
  }

  /**
   * Returns the factory object for this form
   */
  public function getForm() {
    $model = SynchModel::getInstance();
    return $model->getSynchForm($this->formId);
  }

}