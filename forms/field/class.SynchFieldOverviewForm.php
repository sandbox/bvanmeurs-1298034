<?php

class SynchFieldOverviewForm extends SynchForm {

  public function getFormId() {
    return "field_ui_field_overview_form";
  }

  public function createInstance() {
    return new SynchFieldOverviewFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    $targetType = $formState['build_info']['args'][0];
    $targetObj = $formState['build_info']['args'][1];

    switch($targetType) {
      case 'node':
        return t('Configure node type fields for \'!node_type\'', array('!node_type' => $targetObj->type));
      case 'user':
        return t('Configure user account fields');
      default:
        //Unknown
        $desc = t('Configure fields for \'!target_type\'', array('!target_type' => $targetType));
        if (is_string($targetObj)) {
          $desc = $desc . ',\'' . $targetObj . '\'';
        }
        return $desc;
    }
  }

  public function getHelp() {
    return t('Shows the field instances of some entity bundle');
  }

  public function getHelpExceptions() {
    return array(
      t('if a field instance doesn\'t exist locally, it is ignored'),
      t('if a field instance doesn\'t exist remotely, it is not modified'),
    );
  }

}

class SynchFieldOverviewFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'field_ui') . '/field_ui.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    return true;
  }

  public function preSave($finalFormState) {
    parent::preSave($finalFormState);

    //Get the field instance id and add it to the form for local id translation
    if (isset($finalFormState['fields_added'])) {
      $fe = $finalFormState['fields_added'];
      if (isset($fe['_add_existing_field'])) {
        $fid = $this->getFieldInstanceId($finalFormState['complete form']['#entity_type'], $finalFormState['complete form']['#bundle'], $fe['_add_existing_field']);
        $this->info['_add_existing_field_instance_id'] = $fid['field_instance_id'];
        $this->info['_add_existing_field_id'] = $fid['field_id'];
      }

      if (isset($fe['_add_new_field'])) {
      	$fid = $this->getFieldInstanceId($finalFormState['complete form']['#entity_type'], $finalFormState['complete form']['#bundle'], $fe['_add_new_field']);
        $this->info['_add_new_field_instance_id'] = $fid['field_instance_id'];
        $this->info['_add_new_field_id'] = $fid['field_id'];
      }
    }
  }

  /**
   * Returns the id of the specified field instance
   * @return array('field_id' => int, 'field_instance_id' => int)
   */
  private function getFieldInstanceId($entityType, $bundle, $fieldName) {
    $query = db_select('field_config_instance', 'i');
    $query->addField('i', 'id', 'field_instance_id');
    $query->addField('i', 'field_id');
    $query->condition('field_name', $fieldName)
      ->condition('entity_type', $entityType)
      ->condition('bundle', $bundle);
    $result = $query->execute();
    $record = $result->fetchAssoc();
    if (!$record) {
      throw new Exception('Field instance should have been added but does not exist in database: ' . $entityType . ',' . $bundle . ',' . $fieldName);
    }
    return $record;
  }

  public function skip() {
    if (isset($this->info['_add_existing_field_instance_id'])) {
      $this->source->setLocalId('field_instance', $this->info['_add_existing_field_instance_id'], NULL);
    }

    if (isset($this->info['_add_new_field_instance_id'])) {
      $this->source->setLocalId('field_instance', $this->info['_add_new_field_instance_id'], NULL);
      $this->source->setLocalId('field', $this->info['_add_new_field_id'], NULL);
    }
  }

  public function postExec($formState) {
    //In case new field was added, set local id
    if (isset($formState['fields_added'])) {
      $fe = $formState['fields_added'];
      if (isset($this->info['_add_existing_field_instance_id'])) {
        $localFid = $this->getFieldInstanceId($formState['complete form']['#entity_type'], $formState['complete form']['#bundle'], $fe['_add_existing_field']);
        $this->source->setLocalId('field_instance', $this->info['_add_existing_field_instance_id'], $localFid['field_instance_id']);
      }

      if (isset($this->info['_add_new_field_instance_id'])) {
        $localFid = $this->getFieldInstanceId($formState['complete form']['#entity_type'], $formState['complete form']['#bundle'], $fe['_add_new_field']);
        $this->source->setLocalId('field_instance', $this->info['_add_new_field_instance_id'], $localFid['field_instance_id']);
        $this->source->setLocalId('field', $this->info['_add_new_field_id'], $localFid['field_id']);
      }
    }
  }

  public function getAction() {
    $action = $this->form['#action'];

    //@see field_ui_menu
    $type_info = entity_get_info($this->formState['build_info']['args'][0]);
    $entity = $this->formState['build_info']['args'][1];

    if (empty($type_info['entity keys']['bundle'])) {
      $bundle = $type_info;
    } else {
      $bundle = $entity->{$type_info['entity keys']['bundle']};
    }

    $bundle_info = $type_info['bundles'][$bundle];

    $path = $bundle_info['admin']['path'];
    $path_parts = explode('/', $path);

    $path_parts[$bundle_info['admin']['bundle argument']] = $bundle;

    $path_parts[] = 'fields';

    return url(implode('/', $path_parts));
  }

}