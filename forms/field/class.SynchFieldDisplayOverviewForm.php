<?php

class SynchFieldDisplayOverviewForm extends SynchForm {

  public function getFormId() {
    return "field_ui_display_overview_form";
  }

  public function createInstance() {
    return new SynchFieldDisplayOverviewFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    $targetType = $formState['build_info']['args'][0];
    $targetObj = $formState['build_info']['args'][1];
    $targetView = $formState['build_info']['args'][2];

    switch($targetType) {
      case 'node':
        return t('Configure node type field view \'!view\' for \'!node_type\'', array('!view' => $targetView, '!node_type' => $targetObj->type));
      case 'user':
        return t('Configure user account field view \'!view\'', array('!view' => $targetView));
      default:
        //Unknown
        $desc = t('Configure field view \'!view\' for \'!target_type\'', array('!view' => $targetView, '!target_type' => $targetType));
        if (is_string($targetObj)) {
          $desc = $desc . ',\'' . $targetObj . '\'';
        }
        return $desc;
    }
  }

  public function getHelp() {
    return t('Shows the view of all field instances of some entity bundle');
  }

  public function getHelpExceptions() {
    return array(
      t('if a field instance doesn\'t exist locally, it is ignored'),
      t('if a field instance doesn\'t exist remotely, it is not modified'),
    );
  }

}

/**
 * @note Locally unknown field instances are automatically filtered out
 * @note Remotely unknown field instances don't have to be added manually
 */
class SynchFieldDisplayOverviewFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'field_ui') . '/field_ui.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    return true;
  }

  public function getAction() {
    $action = $this->form['#action'];

    //@see field_ui_menu
    $type_info = entity_get_info($this->formState['build_info']['args'][0]);
    $entity = $this->formState['build_info']['args'][1];

    if (empty($type_info['entity keys']['bundle'])) {
      $bundle = $type_info;
    } else {
      $bundle = $entity->{$type_info['entity keys']['bundle']};
    }

    $bundle_info = $type_info['bundles'][$bundle];

    $path = $bundle_info['admin']['path'];
    $path_parts = explode('/', $path);

    $path_parts[$bundle_info['admin']['bundle argument']] = $bundle;

    $path_parts[] = 'display';

    //Add display mode
    if (!empty($this->formState['build_info']['args'][2])) {
      $path_parts[] = $this->formState['build_info']['args'][2];
    }

    return url(implode('/', $path_parts));
  }

}