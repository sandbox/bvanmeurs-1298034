<?php

class SynchBlockAdminConfigureForm extends SynchForm {

  public function getFormId() {
    return "block_admin_configure";
  }

  public function createInstance() {
    return new SynchBlockAdminConfigureFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
  	return t('Configuring block \'!delta\' from module \'!module\'', array('!module' => $formState['build_info']['args'][0], '!delta' => $formState['build_info']['args'][1]));
  }

  public function getHelp() {
    return t('Edits an existing block');
  }

  public function getHelpExceptions() {
    return array(
      t('if the block doesn\'t exist locally, an error is shown'),
    );
  }

}

class SynchBlockAdminConfigureFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'block') . '/block.admin.inc');
  }

  public function getLocalValues() {
    return parent::getLocalValues() + array('module', 'delta');
  }

  public function preExec() {
    if (!parent::preExec()) return false;
    $module = $this->formState['build_info']['args'][0];
    $delta = $this->formState['build_info']['args'][1];

    if ($module == 'block') {
      //Replace remote delta by local delta
      $localDelta = $this->source->getLocalId('block_custom', $delta);
    } else {
      $localDelta = $delta;
    }

    //Check if block exists
    $block = block_load($module, $localDelta);
    if (!isset($block->bid)) {
      //Block does not exist! Don't submit form as it would result in an error
      drupal_set_message(t('Form #!id not submitted because block does not exist: \'!module, !delta.\'', array('!id' => $this->id, '!module' => $module, '!delta' => $localDelta)), 'warning');
      return false;
    }

    //Overwrite remote delta with local delta
    $this->formState['build_info']['args'][1] = $localDelta;

    // @see http://drupal.org/node/1596244
    if ($block->module == 'menu_block') {
      $this->formState['build_info']['files']['menu_block'] = array(
        'type' => 'inc',
        'module' => 'menu_block',
        'name' => 'menu_block.admin'
      );
    }

    return true;
  }

  //@todo: localize roles and content types

  public function getAction() {
    return url("admin/structure/block/manage/{$this->formState['build_info']['args'][0]}/{$this->formState['build_info']['args'][1]}/configure");
  }

}