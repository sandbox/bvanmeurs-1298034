<?php

class SynchBlockCustomBlockDeleteForm extends SynchForm {

  public function getFormId() {
    return "block_custom_block_delete";
  }

  public function createInstance() {
    return new SynchBlockCustomBlockDeleteFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Deleting block \'!delta\' from module \'!module\'', array('!module' => $formState['build_info']['args'][0], '!delta' => $formState['build_info']['args'][1]));
  }

  public function getHelp() {
    return t('Deletes a custom block');
  }

  public function getHelpExceptions() {
    return array(
      t('if the block doesn\'t exist locally, an error is shown'),
    );
  }

}

class SynchBlockCustomBlockDeleteFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'block') . '/block.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Don't redirect after deleting
    unset($this->formState['redirect']);

    //Check block existance and translate remote id to local id

    $module = $this->formState['build_info']['args'][0];
    $delta = $this->formState['build_info']['args'][1];

    if ($module == 'block') {
      //Replace remote delta by local delta
      $localDelta = $this->source->getLocalId('block_custom', $delta);
    } else {
      $localDelta = $this->formState['values']['delta'];
    }

    //Check if block exists
    $block = block_load($module, $localDelta);
    if (!isset($block->bid)) {
      //Block does not exist! Don't submit form as it would result in an error
      drupal_set_message(t('Can\'t submit form #!id not submitted because block does not exist: \'!module, !delta.\'', array('!id' => $this->id, '!module' => $module, '!delta' => $localDelta)), 'error');
      return false;
    }

    $this->formState['build_info']['args'][1] = $localDelta;

    //Replace bid
    $this->formState['inputs']['bid'] = $block->bid;
    $this->formState['values']['bid'] = $block->bid;
    $this->formState['comple_form']['bid'] = $block->bid;

    return true;
  }

  public function getAction() {
    return url("admin/structure/block/manage/{$this->formState['build_info']['args'][0]}/{$this->formState['build_info']['args'][1]}/delete");
  }

}