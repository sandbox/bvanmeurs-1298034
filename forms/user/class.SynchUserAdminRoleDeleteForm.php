<?php

class SynchUserAdminRoleDeleteForm extends SynchForm {

  public function getFormId() {
    return "user_admin_role_delete_confirm";
  }

  public function createInstance() {
    return new SynchUserAdminRoleDeleteFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    //Deleting role
    return t('Deleting role \'!role_name\'', array('!role_name' => $formState['build_info']['args'][0]->rid));
  }

  public function getHelp() {
    return t('Delete a role');
  }

  public function getHelpExceptions() {
    return array(
      t('if the role does not exist locally, an error is shown'),
    );
  }

}

class SynchUserAdminRoleDeleteFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'user') . '/user.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Get local id
    $localId = $this->source->getLocalId('role', $this->formState['build_info']['args'][0]->rid);
    $localRole = user_role_load($localId);

    //Check role correctness
    if (!$localRole) {
      //Role does not exist!
      drupal_set_message(t('Form #!id not submitted because user role does not exist: \'!rid\' (!role_name).', array('!id' => $this->formId, '!rid' => $localId, '!role_name' => $this->formState['build_info']['args'][0]->name)), 'warning');
      return false;
    }

    //Set role
    $this->formState['build_info']['args'][0] = $localRole;
    $this->formState['values']['rid'] = $localId;

    return true;
  }

  public function getAction() {
    return url("admin/people/permissions/roles/delete/{$this->formState['build_info']['args'][0]->rid}");
  }

}