<?php

class SynchUserAdminRoleForm extends SynchForm {

  public function getFormId() {
    return "user_admin_role";
  }

  public function createInstance() {
    return new SynchUserAdminRoleFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    //Changing role
    return t('Changing role \'!role_name\'', array('!role_name' => $formState['build_info']['args'][0]->rid));
  }

  public function getHelp() {
    return t('Edit a role');
  }

  public function getHelpExceptions() {
    return array(
      t('if the role does not exist locally, an error is shown'),
    );
  }

}

class SynchUserAdminRoleFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'user') . '/user.admin.inc');
  }

  public function ignore($finalFormState) {
    if (!empty($finalFormState['triggering_element']['#value']) &&
      $finalFormState['triggering_element']['#value'] == t('Delete role')) {
      //The delete button was pressed and the confirmation form is being built. As nothing is done until the confirmation button is clicked, we can ignore this form.
      return TRUE;
    }
    return FALSE;
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Get local id
    $localId = $this->source->getLocalId('role', $this->formState['build_info']['args'][0]->rid);
    $localRole = user_role_load($localId);

    //Check role correctness
    if (!$localRole) {
      //Role does not exist!
      drupal_set_message(t('Form #!id not submitted because user role does not exist: \'!rid\' (!role_name).', array('!id' => $this->formId, '!rid' => $localId, '!role_name' => $this->formState['build_info']['args'][0]->name)), 'warning');
      return false;
    }

    //Set role
    $this->formState['build_info']['args'][0] = $localRole;

    return true;
  }

  public function getAction() {
    return url("admin/people/permissions/roles/edit/{$this->formState['build_info']['args'][0]->rid}");
  }
}