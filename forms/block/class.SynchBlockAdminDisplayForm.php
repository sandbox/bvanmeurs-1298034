<?php

class SynchBlockAdminDisplayForm extends SynchForm {

  public function getFormId() {
    return "block_admin_display_form";
  }

  public function createInstance() {
    return new SynchBlockAdminDisplayFormSubmit();
  }

  public function getDefaultSynchDescription($formState) {
    return t('Save region and order of all blocks.');
  }

  public function getHelp() {
    return t('Edits the block order');
  }

  public function getHelpExceptions() {
    return array(
      t('if a block doesn\'t exist locally, it is ignored and a warning is shown'),
      t('if a block doesn\'t exist remotely, it will not be modified'),
    );
  }

}

class SynchBlockAdminDisplayFormSubmit extends SynchFormSubmit {

  public function getRequiredIncludes() {
    return array(drupal_get_path('module', 'block') . '/block.admin.inc');
  }

  public function preExec() {
    if (!parent::preExec()) return false;

    //Check all blocks
    $model = SynchModel::getInstance();
    foreach ($this->formState['values']['blocks'] as $key => $vBlock) {
      $module = $vBlock['module'];
      $delta = $vBlock['delta'];

      if ($module == 'block') {
        //Translate to local delta
        $delta = $this->source->getLocalId('block_custom', $delta);
      }

      //Check if block exists
      $block = block_load($module, $delta);
      if (!isset($block->bid)) {
        //Block does not exist! Ignore block
        drupal_set_message(t('Form #!id, block !module,!delta (local: !module,!local_delta) is ignored because it does not exist: \'!module, !delta.\'', array('!id' => $this->id, '!module' => $module, '!delta' => $vBlock['delta'], '!local_delta' => $delta)), 'warning');
      } else {
        if ($delta != $vBlock['delta']) {
          //Change delta in form input
          unset($this->formState['values']['blocks'][$key]);
          unset($this->formState['inputs']['blocks'][$key]);

          $vBlock['delta'] = $delta;
          $this->formState['values']['blocks'][$module . '_' . $delta] = $vBlock;
          $this->formState['inputs']['blocks'][$module . '_' . $delta] = $vBlock;
        }
      }
    }

    return true;
  }

  public function getAction() {
    return url("admin/structure/block");
  }

}